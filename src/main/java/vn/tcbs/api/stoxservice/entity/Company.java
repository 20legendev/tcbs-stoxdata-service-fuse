package vn.tcbs.api.stoxservice.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the stox_tb_Company database table.
 * 
 */
@Entity
@Table(name="stox_tb_Company")
@NamedQuery(name="Company.findAll", query="SELECT c FROM Company c")
public class Company implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private Integer id;

	@Column(name="BusinessRisk")
	private String businessRisk;

	@Column(name="BusinessStrategies")
	private String businessStrategies;

	@Column(name="CapitalFund")
	private Double capitalFund;

	@Column(name="CityID")
	private String cityID;

	@Column(name="CompanyProfile")
	private String companyProfile;

	@Column(name="CompanyPromise")
	private String companyPromise;

	@Column(name="CompanyTypeBeforeListed")
	private String companyTypeBeforeListed;

	@Column(name="CreateDate")
	private Timestamp createDate;

	@Column(name="CTCKID")
	private String ctckid;

	@Column(name="DistrictID")
	private String districtID;

	private String eBusinessRisk;

	private String eBusinessStrategies;

	private String eCityID;

	private String eCompanyProfile;

	private String eCompanyPromise;

	private String eDistrictID;

	private String eHistoryDev;

	private String eKeyDevelopments;

	private String eLargeInvestProject;

	@Column(name="EnglishName")
	private String englishName;

	private String ePosCompany;

	private String ePrimaryProduct;

	private String eRegistedOffice;

	@Column(name="ExchangeID")
	private short exchangeID;

	@Column(name="F2_74")
	private Double f274;

	@Column(name="Fax_AC")
	private String fax_AC;

	@Column(name="Fax_CC")
	private String fax_CC;

	@Column(name="Fax_Number")
	private String fax_Number;

	@Column(name="FirstListingDate")
	private Timestamp firstListingDate;

	@Column(name="FirstVolumn")
	private Double firstVolumn;

	@Column(name="Freefloat")
	private Double freefloat;

	@Column(name="HistoryDev")
	private String historyDev;

	@Column(name="[Index]")
	private String index;

	@Column(name="IndustryID")
	private String industryID;

	@Column(name="IndustryParent")
	private String industryParent;

	private String jpBusinessRisk;

	private String jpBusinessStrategies;

	private String jpCityID;

	private String jpCompanyProfile;

	private String jpCompanyPromise;

	private String jpDistrictID;

	private String jpHistoryDev;

	private String jpKeyDevelopments;

	private String jpLargeInvestProject;

	private String jpPosCompany;

	private String jpRegistedOffice;

	@Column(name="KeyDevelopments")
	private String keyDevelopments;

	@Column(name="LargeInvestProject")
	private String largeInvestProject;

	@Column(name="MarketCapIndex")
	private Integer marketCapIndex;

	@Column(name="MarketCapIndex_new", nullable=true)
	private Integer marketCapIndex_new;

	@Column(name="Name")
	private String name;

	@Column(name="NumberOfATM")
	private String numberOfATM;

	@Column(name="NumberOfCDPT")
	private Double numberOfCDPT;

	@Column(name="NumberOfEmployees")
	private Double numberOfEmployees;

	@Column(name="NumberOfSub1")
	private String numberOfSub1;

	@Column(name="NumberOfTradeStation")
	private String numberOfTradeStation;

	@Column(name="ParValue")
	private Double parValue;

	@Column(name="PosCompany")
	private String posCompany;

	@Column(name="PriceListed")
	private Double priceListed;

	@Column(name="PrimaryProduct")
	private String primaryProduct;

	@Column(name="RegistedOffice")
	private String registedOffice;

	@Column(name="ShareCirculate")
	private Double shareCirculate;

	@Column(name="ShareIssue")
	private Double shareIssue;

	@Column(name="ShareType")
	private String shareType;

	@Column(name="ShortEnglishName")
	private String shortEnglishName;

	@Column(name="ShortName")
	private String shortName;

	@Column(name="Telephone_AC")
	private String telephone_AC;

	@Column(name="Telephone_CC")
	private String telephone_CC;

	@Column(name="Telephone_Number")
	private String telephone_Number;

	@Column(name="Ticker")
	private String ticker;

	@Column(name="TypeID")
	private short typeID;

	@Column(name="WebsiteAboutCompany")
	private String websiteAboutCompany;

	@Column(name="WebsiteCorporateAction")
	private String websiteCorporateAction;

	@Column(name="WebsiteHomepage")
	private String websiteHomepage;

	@Column(name="WebsiteInvesterRelation")
	private String websiteInvesterRelation;

	@Column(name="WebsiteJobHunter")
	private String websiteJobHunter;

	@Column(name="WebsiteProductServices")
	private String websiteProductServices;

	public Company() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBusinessRisk() {
		return this.businessRisk;
	}

	public void setBusinessRisk(String businessRisk) {
		this.businessRisk = businessRisk;
	}

	public String getBusinessStrategies() {
		return this.businessStrategies;
	}

	public void setBusinessStrategies(String businessStrategies) {
		this.businessStrategies = businessStrategies;
	}

	public Double getCapitalFund() {
		return this.capitalFund;
	}

	public void setCapitalFund(Double capitalFund) {
		this.capitalFund = capitalFund;
	}

	public String getCityID() {
		return this.cityID;
	}

	public void setCityID(String cityID) {
		this.cityID = cityID;
	}

	public String getCompanyProfile() {
		return this.companyProfile;
	}

	public void setCompanyProfile(String companyProfile) {
		this.companyProfile = companyProfile;
	}

	public String getCompanyPromise() {
		return this.companyPromise;
	}

	public void setCompanyPromise(String companyPromise) {
		this.companyPromise = companyPromise;
	}

	public String getCompanyTypeBeforeListed() {
		return this.companyTypeBeforeListed;
	}

	public void setCompanyTypeBeforeListed(String companyTypeBeforeListed) {
		this.companyTypeBeforeListed = companyTypeBeforeListed;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getCtckid() {
		return this.ctckid;
	}

	public void setCtckid(String ctckid) {
		this.ctckid = ctckid;
	}

	public String getDistrictID() {
		return this.districtID;
	}

	public void setDistrictID(String districtID) {
		this.districtID = districtID;
	}

	public String getEBusinessRisk() {
		return this.eBusinessRisk;
	}

	public void setEBusinessRisk(String eBusinessRisk) {
		this.eBusinessRisk = eBusinessRisk;
	}

	public String getEBusinessStrategies() {
		return this.eBusinessStrategies;
	}

	public void setEBusinessStrategies(String eBusinessStrategies) {
		this.eBusinessStrategies = eBusinessStrategies;
	}

	public String getECityID() {
		return this.eCityID;
	}

	public void setECityID(String eCityID) {
		this.eCityID = eCityID;
	}

	public String getECompanyProfile() {
		return this.eCompanyProfile;
	}

	public void setECompanyProfile(String eCompanyProfile) {
		this.eCompanyProfile = eCompanyProfile;
	}

	public String getECompanyPromise() {
		return this.eCompanyPromise;
	}

	public void setECompanyPromise(String eCompanyPromise) {
		this.eCompanyPromise = eCompanyPromise;
	}

	public String getEDistrictID() {
		return this.eDistrictID;
	}

	public void setEDistrictID(String eDistrictID) {
		this.eDistrictID = eDistrictID;
	}

	public String getEHistoryDev() {
		return this.eHistoryDev;
	}

	public void setEHistoryDev(String eHistoryDev) {
		this.eHistoryDev = eHistoryDev;
	}

	public String getEKeyDevelopments() {
		return this.eKeyDevelopments;
	}

	public void setEKeyDevelopments(String eKeyDevelopments) {
		this.eKeyDevelopments = eKeyDevelopments;
	}

	public String getELargeInvestProject() {
		return this.eLargeInvestProject;
	}

	public void setELargeInvestProject(String eLargeInvestProject) {
		this.eLargeInvestProject = eLargeInvestProject;
	}

	public String getEnglishName() {
		return this.englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getEPosCompany() {
		return this.ePosCompany;
	}

	public void setEPosCompany(String ePosCompany) {
		this.ePosCompany = ePosCompany;
	}

	public String getEPrimaryProduct() {
		return this.ePrimaryProduct;
	}

	public void setEPrimaryProduct(String ePrimaryProduct) {
		this.ePrimaryProduct = ePrimaryProduct;
	}

	public String getERegistedOffice() {
		return this.eRegistedOffice;
	}

	public void setERegistedOffice(String eRegistedOffice) {
		this.eRegistedOffice = eRegistedOffice;
	}

	public short getExchangeID() {
		return this.exchangeID;
	}

	public void setExchangeID(short exchangeID) {
		this.exchangeID = exchangeID;
	}

	public Double getF274() {
		return this.f274;
	}

	public void setF274(Double f274) {
		this.f274 = f274;
	}

	public String getFax_AC() {
		return this.fax_AC;
	}

	public void setFax_AC(String fax_AC) {
		this.fax_AC = fax_AC;
	}

	public String getFax_CC() {
		return this.fax_CC;
	}

	public void setFax_CC(String fax_CC) {
		this.fax_CC = fax_CC;
	}

	public String getFax_Number() {
		return this.fax_Number;
	}

	public void setFax_Number(String fax_Number) {
		this.fax_Number = fax_Number;
	}

	public Timestamp getFirstListingDate() {
		return this.firstListingDate;
	}

	public void setFirstListingDate(Timestamp firstListingDate) {
		this.firstListingDate = firstListingDate;
	}

	public Double getFirstVolumn() {
		return this.firstVolumn;
	}

	public void setFirstVolumn(Double firstVolumn) {
		this.firstVolumn = firstVolumn;
	}

	public Double getFreefloat() {
		return this.freefloat;
	}

	public void setFreefloat(Double freefloat) {
		this.freefloat = freefloat;
	}

	public String getHistoryDev() {
		return this.historyDev;
	}

	public void setHistoryDev(String historyDev) {
		this.historyDev = historyDev;
	}

	public String getIndex() {
		return this.index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getIndustryID() {
		return this.industryID;
	}

	public void setIndustryID(String industryID) {
		this.industryID = industryID;
	}

	public String getIndustryParent() {
		return this.industryParent;
	}

	public void setIndustryParent(String industryParent) {
		this.industryParent = industryParent;
	}

	public String getJpBusinessRisk() {
		return this.jpBusinessRisk;
	}

	public void setJpBusinessRisk(String jpBusinessRisk) {
		this.jpBusinessRisk = jpBusinessRisk;
	}

	public String getJpBusinessStrategies() {
		return this.jpBusinessStrategies;
	}

	public void setJpBusinessStrategies(String jpBusinessStrategies) {
		this.jpBusinessStrategies = jpBusinessStrategies;
	}

	public String getJpCityID() {
		return this.jpCityID;
	}

	public void setJpCityID(String jpCityID) {
		this.jpCityID = jpCityID;
	}

	public String getJpCompanyProfile() {
		return this.jpCompanyProfile;
	}

	public void setJpCompanyProfile(String jpCompanyProfile) {
		this.jpCompanyProfile = jpCompanyProfile;
	}

	public String getJpCompanyPromise() {
		return this.jpCompanyPromise;
	}

	public void setJpCompanyPromise(String jpCompanyPromise) {
		this.jpCompanyPromise = jpCompanyPromise;
	}

	public String getJpDistrictID() {
		return this.jpDistrictID;
	}

	public void setJpDistrictID(String jpDistrictID) {
		this.jpDistrictID = jpDistrictID;
	}

	public String getJpHistoryDev() {
		return this.jpHistoryDev;
	}

	public void setJpHistoryDev(String jpHistoryDev) {
		this.jpHistoryDev = jpHistoryDev;
	}

	public String getJpKeyDevelopments() {
		return this.jpKeyDevelopments;
	}

	public void setJpKeyDevelopments(String jpKeyDevelopments) {
		this.jpKeyDevelopments = jpKeyDevelopments;
	}

	public String getJpLargeInvestProject() {
		return this.jpLargeInvestProject;
	}

	public void setJpLargeInvestProject(String jpLargeInvestProject) {
		this.jpLargeInvestProject = jpLargeInvestProject;
	}

	public String getJpPosCompany() {
		return this.jpPosCompany;
	}

	public void setJpPosCompany(String jpPosCompany) {
		this.jpPosCompany = jpPosCompany;
	}

	public String getJpRegistedOffice() {
		return this.jpRegistedOffice;
	}

	public void setJpRegistedOffice(String jpRegistedOffice) {
		this.jpRegistedOffice = jpRegistedOffice;
	}

	public String getKeyDevelopments() {
		return this.keyDevelopments;
	}

	public void setKeyDevelopments(String keyDevelopments) {
		this.keyDevelopments = keyDevelopments;
	}

	public String getLargeInvestProject() {
		return this.largeInvestProject;
	}

	public void setLargeInvestProject(String largeInvestProject) {
		this.largeInvestProject = largeInvestProject;
	}

	public Integer getMarketCapIndex() {
		return this.marketCapIndex;
	}

	public void setMarketCapIndex(Integer marketCapIndex) {
		this.marketCapIndex = marketCapIndex;
	}

	public Integer getMarketCapIndex_new() {
		return this.marketCapIndex_new;
	}

	public void setMarketCapIndex_new(Integer marketCapIndex_new) {
		this.marketCapIndex_new = marketCapIndex_new;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumberOfATM() {
		return this.numberOfATM;
	}

	public void setNumberOfATM(String numberOfATM) {
		this.numberOfATM = numberOfATM;
	}

	public Double getNumberOfCDPT() {
		return this.numberOfCDPT;
	}

	public void setNumberOfCDPT(Double numberOfCDPT) {
		this.numberOfCDPT = numberOfCDPT;
	}

	public Double getNumberOfEmployees() {
		return this.numberOfEmployees;
	}

	public void setNumberOfEmployees(Double numberOfEmployees) {
		this.numberOfEmployees = numberOfEmployees;
	}

	public String getNumberOfSub1() {
		return this.numberOfSub1;
	}

	public void setNumberOfSub1(String numberOfSub1) {
		this.numberOfSub1 = numberOfSub1;
	}

	public String getNumberOfTradeStation() {
		return this.numberOfTradeStation;
	}

	public void setNumberOfTradeStation(String numberOfTradeStation) {
		this.numberOfTradeStation = numberOfTradeStation;
	}

	public Double getParValue() {
		return this.parValue;
	}

	public void setParValue(Double parValue) {
		this.parValue = parValue;
	}

	public String getPosCompany() {
		return this.posCompany;
	}

	public void setPosCompany(String posCompany) {
		this.posCompany = posCompany;
	}

	public Double getPriceListed() {
		return this.priceListed;
	}

	public void setPriceListed(Double priceListed) {
		this.priceListed = priceListed;
	}

	public String getPrimaryProduct() {
		return this.primaryProduct;
	}

	public void setPrimaryProduct(String primaryProduct) {
		this.primaryProduct = primaryProduct;
	}

	public String getRegistedOffice() {
		return this.registedOffice;
	}

	public void setRegistedOffice(String registedOffice) {
		this.registedOffice = registedOffice;
	}

	public Double getShareCirculate() {
		return this.shareCirculate;
	}

	public void setShareCirculate(Double shareCirculate) {
		this.shareCirculate = shareCirculate;
	}

	public Double getShareIssue() {
		return this.shareIssue;
	}

	public void setShareIssue(Double shareIssue) {
		this.shareIssue = shareIssue;
	}

	public String getShareType() {
		return this.shareType;
	}

	public void setShareType(String shareType) {
		this.shareType = shareType;
	}

	public String getShortEnglishName() {
		return this.shortEnglishName;
	}

	public void setShortEnglishName(String shortEnglishName) {
		this.shortEnglishName = shortEnglishName;
	}

	public String getShortName() {
		return this.shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getTelephone_AC() {
		return this.telephone_AC;
	}

	public void setTelephone_AC(String telephone_AC) {
		this.telephone_AC = telephone_AC;
	}

	public String getTelephone_CC() {
		return this.telephone_CC;
	}

	public void setTelephone_CC(String telephone_CC) {
		this.telephone_CC = telephone_CC;
	}

	public String getTelephone_Number() {
		return this.telephone_Number;
	}

	public void setTelephone_Number(String telephone_Number) {
		this.telephone_Number = telephone_Number;
	}

	public String getTicker() {
		return this.ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public short getTypeID() {
		return this.typeID;
	}

	public void setTypeID(short typeID) {
		this.typeID = typeID;
	}

	public String getWebsiteAboutCompany() {
		return this.websiteAboutCompany;
	}

	public void setWebsiteAboutCompany(String websiteAboutCompany) {
		this.websiteAboutCompany = websiteAboutCompany;
	}

	public String getWebsiteCorporateAction() {
		return this.websiteCorporateAction;
	}

	public void setWebsiteCorporateAction(String websiteCorporateAction) {
		this.websiteCorporateAction = websiteCorporateAction;
	}

	public String getWebsiteHomepage() {
		return this.websiteHomepage;
	}

	public void setWebsiteHomepage(String websiteHomepage) {
		this.websiteHomepage = websiteHomepage;
	}

	public String getWebsiteInvesterRelation() {
		return this.websiteInvesterRelation;
	}

	public void setWebsiteInvesterRelation(String websiteInvesterRelation) {
		this.websiteInvesterRelation = websiteInvesterRelation;
	}

	public String getWebsiteJobHunter() {
		return this.websiteJobHunter;
	}

	public void setWebsiteJobHunter(String websiteJobHunter) {
		this.websiteJobHunter = websiteJobHunter;
	}

	public String getWebsiteProductServices() {
		return this.websiteProductServices;
	}

	public void setWebsiteProductServices(String websiteProductServices) {
		this.websiteProductServices = websiteProductServices;
	}

}