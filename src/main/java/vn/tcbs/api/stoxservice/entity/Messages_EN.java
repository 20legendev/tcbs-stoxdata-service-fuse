package vn.tcbs.api.stoxservice.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;


/**
 * The persistent class for the Messages_EN database table.
 * 
 */
@Entity
@NamedQuery(name="Messages_EN.findAll", query="SELECT m FROM Messages_EN m")
public class Messages_EN implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="MessageID")
	private long messageID;

	@Column(name="Authority")
	private String authority;

	@Column(name="BusinessCheckNews")
	private short businessCheckNews;

	@Column(name="CategoryID")
	private int categoryID;

	@Column(name="ClickOnPostDate")
	private int clickOnPostDate;

	@Column(name="CompanyID")
	private int companyID;

	@Column(name="IndustryID")
	private int industryID;

	@Column(name="IsAnalysis")
	private boolean isAnalysis;

	@Column(name="IsAudio")
	private boolean isAudio;

	@Column(name="IsCutOff")
	private boolean isCutOff;

	private boolean isExchangeNews;

	@Column(name="IsFeedback")
	private boolean isFeedback;

	@Column(name="IsHome")
	private boolean isHome;

	private boolean isHomeShow;

	@Column(name="IsImportant")
	private boolean isImportant;

	@Column(name="IsLinkTicker")
	private boolean isLinkTicker;

	@Column(name="IsMarketOne")
	private boolean isMarketOne;

	@Column(name="IsMarketThree")
	private boolean isMarketThree;

	@Column(name="IsMarketTwo")
	private boolean isMarketTwo;

	@Column(name="IsSalient")
	private boolean isSalient;

	@Column(name="IsSendToMember")
	private boolean isSendToMember;

	@Column(name="IsSolution")
	private boolean isSolution;

	@Column(name="IsSpecial")
	private boolean isSpecial;

	@Column(name="IsStockHot")
	private boolean isStockHot;

	@Column(name="IsTop")
	private boolean isTop;

	@Column(name="IsTransfer")
	private boolean isTransfer;

	@Column(name="LastModify")
	private String lastModify;

	@Column(name="MsgAlign")
	private short msgAlign;

	@Column(name="MsgAuthor")
	private String msgAuthor;

	@Column(name="MsgContent")
	private String msgContent;

	@Column(name="MsgHistory")
	private String msgHistory;

	@Column(name="MsgHome")
	private String msgHome;

	@Column(name="MsgHomeImage")
	private String msgHomeImage;

	@Lob
	@Column(name="MsgHomeImage_Byte")
	private byte[] msgHomeImage_Byte;

	@Column(name="MsgImageComment")
	private String msgImageComment;

	@Column(name="MsgLead")
	private String msgLead;

	@Column(name="MsgOrder")
	private int msgOrder;

	@Column(name="MsgPostDate")
	private Date msgPostDate;

	@Column(name="MsgSmallImage")
	private String msgSmallImage;

	@Lob
	@Column(name="MsgSmallImage_Byte")
	private byte[] msgSmallImage_Byte;

	@Column(name="MsgStatus")
	private short msgStatus;

	@Column(name="MsgSubTitle")
	private String msgSubTitle;

	@Column(name="MsgTitle")
	private String msgTitle;

	@Column(name="MsgTitleReference")
	private String msgTitleReference;

	@Column(name="MsgType")
	private String msgType;

	@Column(name="Position")
	private int position;

	@Column(name="Poster")
	private String poster;

	@Column(name="RedirectLink")
	private String redirectLink;

	@Column(name="ReferenceID")
	private int referenceID;

	@Column(name="ReportTitle")
	private String reportTitle;

	@Column(name="Royalties")
	private double royalties;

	@Column(name="StoxID")
	private long stoxID;

	@Column(name="Ticker")
	private String ticker;

	@Column(name="TotalClick")
	private int totalClick;

	public Messages_EN() {
	}

	public long getMessageID() {
		return this.messageID;
	}

	public void setMessageID(long messageID) {
		this.messageID = messageID;
	}

	public String getAuthority() {
		return this.authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public short getBusinessCheckNews() {
		return this.businessCheckNews;
	}

	public void setBusinessCheckNews(short businessCheckNews) {
		this.businessCheckNews = businessCheckNews;
	}

	public int getCategoryID() {
		return this.categoryID;
	}

	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}

	public int getClickOnPostDate() {
		return this.clickOnPostDate;
	}

	public void setClickOnPostDate(int clickOnPostDate) {
		this.clickOnPostDate = clickOnPostDate;
	}

	public int getCompanyID() {
		return this.companyID;
	}

	public void setCompanyID(int companyID) {
		this.companyID = companyID;
	}

	public int getIndustryID() {
		return this.industryID;
	}

	public void setIndustryID(int industryID) {
		this.industryID = industryID;
	}

	public boolean getIsAnalysis() {
		return this.isAnalysis;
	}

	public void setIsAnalysis(boolean isAnalysis) {
		this.isAnalysis = isAnalysis;
	}

	public boolean getIsAudio() {
		return this.isAudio;
	}

	public void setIsAudio(boolean isAudio) {
		this.isAudio = isAudio;
	}

	public boolean getIsCutOff() {
		return this.isCutOff;
	}

	public void setIsCutOff(boolean isCutOff) {
		this.isCutOff = isCutOff;
	}

	public boolean getIsExchangeNews() {
		return this.isExchangeNews;
	}

	public void setIsExchangeNews(boolean isExchangeNews) {
		this.isExchangeNews = isExchangeNews;
	}

	public boolean getIsFeedback() {
		return this.isFeedback;
	}

	public void setIsFeedback(boolean isFeedback) {
		this.isFeedback = isFeedback;
	}

	public boolean getIsHome() {
		return this.isHome;
	}

	public void setIsHome(boolean isHome) {
		this.isHome = isHome;
	}

	public boolean getIsHomeShow() {
		return this.isHomeShow;
	}

	public void setIsHomeShow(boolean isHomeShow) {
		this.isHomeShow = isHomeShow;
	}

	public boolean getIsImportant() {
		return this.isImportant;
	}

	public void setIsImportant(boolean isImportant) {
		this.isImportant = isImportant;
	}

	public boolean getIsLinkTicker() {
		return this.isLinkTicker;
	}

	public void setIsLinkTicker(boolean isLinkTicker) {
		this.isLinkTicker = isLinkTicker;
	}

	public boolean getIsMarketOne() {
		return this.isMarketOne;
	}

	public void setIsMarketOne(boolean isMarketOne) {
		this.isMarketOne = isMarketOne;
	}

	public boolean getIsMarketThree() {
		return this.isMarketThree;
	}

	public void setIsMarketThree(boolean isMarketThree) {
		this.isMarketThree = isMarketThree;
	}

	public boolean getIsMarketTwo() {
		return this.isMarketTwo;
	}

	public void setIsMarketTwo(boolean isMarketTwo) {
		this.isMarketTwo = isMarketTwo;
	}

	public boolean getIsSalient() {
		return this.isSalient;
	}

	public void setIsSalient(boolean isSalient) {
		this.isSalient = isSalient;
	}

	public boolean getIsSendToMember() {
		return this.isSendToMember;
	}

	public void setIsSendToMember(boolean isSendToMember) {
		this.isSendToMember = isSendToMember;
	}

	public boolean getIsSolution() {
		return this.isSolution;
	}

	public void setIsSolution(boolean isSolution) {
		this.isSolution = isSolution;
	}

	public boolean getIsSpecial() {
		return this.isSpecial;
	}

	public void setIsSpecial(boolean isSpecial) {
		this.isSpecial = isSpecial;
	}

	public boolean getIsStockHot() {
		return this.isStockHot;
	}

	public void setIsStockHot(boolean isStockHot) {
		this.isStockHot = isStockHot;
	}

	public boolean getIsTop() {
		return this.isTop;
	}

	public void setIsTop(boolean isTop) {
		this.isTop = isTop;
	}

	public boolean getIsTransfer() {
		return this.isTransfer;
	}

	public void setIsTransfer(boolean isTransfer) {
		this.isTransfer = isTransfer;
	}

	public String getLastModify() {
		return this.lastModify;
	}

	public void setLastModify(String lastModify) {
		this.lastModify = lastModify;
	}

	public short getMsgAlign() {
		return this.msgAlign;
	}

	public void setMsgAlign(short msgAlign) {
		this.msgAlign = msgAlign;
	}

	public String getMsgAuthor() {
		return this.msgAuthor;
	}

	public void setMsgAuthor(String msgAuthor) {
		this.msgAuthor = msgAuthor;
	}

	public String getMsgContent() {
		return this.msgContent;
	}

	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}

	public String getMsgHistory() {
		return this.msgHistory;
	}

	public void setMsgHistory(String msgHistory) {
		this.msgHistory = msgHistory;
	}

	public String getMsgHome() {
		return this.msgHome;
	}

	public void setMsgHome(String msgHome) {
		this.msgHome = msgHome;
	}

	public String getMsgHomeImage() {
		return this.msgHomeImage;
	}

	public void setMsgHomeImage(String msgHomeImage) {
		this.msgHomeImage = msgHomeImage;
	}

	public byte[] getMsgHomeImage_Byte() {
		return this.msgHomeImage_Byte;
	}

	public void setMsgHomeImage_Byte(byte[] msgHomeImage_Byte) {
		this.msgHomeImage_Byte = msgHomeImage_Byte;
	}

	public String getMsgImageComment() {
		return this.msgImageComment;
	}

	public void setMsgImageComment(String msgImageComment) {
		this.msgImageComment = msgImageComment;
	}

	public String getMsgLead() {
		return this.msgLead;
	}

	public void setMsgLead(String msgLead) {
		this.msgLead = msgLead;
	}

	public int getMsgOrder() {
		return this.msgOrder;
	}

	public void setMsgOrder(int msgOrder) {
		this.msgOrder = msgOrder;
	}

	public Date getMsgPostDate() {
		return this.msgPostDate;
	}

	public void setMsgPostDate(Date msgPostDate) {
		this.msgPostDate = msgPostDate;
	}

	public String getMsgSmallImage() {
		return this.msgSmallImage;
	}

	public void setMsgSmallImage(String msgSmallImage) {
		this.msgSmallImage = msgSmallImage;
	}

	public byte[] getMsgSmallImage_Byte() {
		return this.msgSmallImage_Byte;
	}

	public void setMsgSmallImage_Byte(byte[] msgSmallImage_Byte) {
		this.msgSmallImage_Byte = msgSmallImage_Byte;
	}

	public short getMsgStatus() {
		return this.msgStatus;
	}

	public void setMsgStatus(short msgStatus) {
		this.msgStatus = msgStatus;
	}

	public String getMsgSubTitle() {
		return this.msgSubTitle;
	}

	public void setMsgSubTitle(String msgSubTitle) {
		this.msgSubTitle = msgSubTitle;
	}

	public String getMsgTitle() {
		return this.msgTitle;
	}

	public void setMsgTitle(String msgTitle) {
		this.msgTitle = msgTitle;
	}

	public String getMsgTitleReference() {
		return this.msgTitleReference;
	}

	public void setMsgTitleReference(String msgTitleReference) {
		this.msgTitleReference = msgTitleReference;
	}

	public String getMsgType() {
		return this.msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public int getPosition() {
		return this.position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getPoster() {
		return this.poster;
	}

	public void setPoster(String poster) {
		this.poster = poster;
	}

	public String getRedirectLink() {
		return this.redirectLink;
	}

	public void setRedirectLink(String redirectLink) {
		this.redirectLink = redirectLink;
	}

	public int getReferenceID() {
		return this.referenceID;
	}

	public void setReferenceID(int referenceID) {
		this.referenceID = referenceID;
	}

	public String getReportTitle() {
		return this.reportTitle;
	}

	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}

	public double getRoyalties() {
		return this.royalties;
	}

	public void setRoyalties(double royalties) {
		this.royalties = royalties;
	}

	public long getStoxID() {
		return this.stoxID;
	}

	public void setStoxID(long stoxID) {
		this.stoxID = stoxID;
	}

	public String getTicker() {
		return this.ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public int getTotalClick() {
		return this.totalClick;
	}

	public void setTotalClick(int totalClick) {
		this.totalClick = totalClick;
	}

}