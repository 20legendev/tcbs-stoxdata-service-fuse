package vn.tcbs.api.stoxservice.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Type;


/**
 * The persistent class for the stox_tb_corp_DividendYield database table.
 * 
 */
@Entity
@Table(name="stox_tb_corp_DividendYield")
@NamedQuery(name="DividendYield.findAll", query="SELECT d FROM DividendYield d")
public class DividendYield implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Cotuccuanam")
	private double cotuccuanam;

	@Id
	@Column(name="ID")
	private int id;

	@Type(type="string")
	@Column(name="Note")
	private String note;

	@Column(name="Ticker")
	private String ticker;

	@Column(name="Tyletracotuc")
	private double tyletracotuc;

	@Column(name="YearReport")
	private double yearReport;

	public DividendYield() {
	}

	public double getCotuccuanam() {
		return this.cotuccuanam;
	}

	public void setCotuccuanam(double cotuccuanam) {
		this.cotuccuanam = cotuccuanam;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getTicker() {
		return this.ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public double getTyletracotuc() {
		return this.tyletracotuc;
	}

	public void setTyletracotuc(double tyletracotuc) {
		this.tyletracotuc = tyletracotuc;
	}

	public double getYearReport() {
		return this.yearReport;
	}

	public void setYearReport(double yearReport) {
		this.yearReport = yearReport;
	}

}