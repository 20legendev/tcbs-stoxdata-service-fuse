package vn.tcbs.api.stoxservice.entity;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Type;


/**
 * The persistent class for the Categories database table.
 * 
 */
@Entity
@Table(name="Categories")
@NamedQuery(name="Category.findAll", query="SELECT c FROM Category c")
public class Category implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CategoryID")
	private Integer categoryID;
	
	@Type(type="string")
	@Column(name="CategoryAlias")
	private Object categoryAlias;

	@Column(name="CategoryLevel")
	private short categoryLevel;

	@Type(type="string")
	@Column(name="CategoryName")
	private Object categoryName;

	@Type(type="string")
	@Column(name="CategoryName_EN")
	private Object categoryName_EN;

	@Type(type="string")
	@Column(name="CategoryOrder")
	private Object categoryOrder;

	@Column(name="CategoryParentID")
	private Integer categoryParentID;

	@Type(type="string")
	@Column(name="CategoryText")
	private Object categoryText;

	@Column(name="msrepl_tran_version")
	private String msreplTranVersion;

	public Category() {
	}

	public Integer getCategoryID() {
		return this.categoryID;
	}

	public void setCategoryID(Integer categoryID) {
		this.categoryID = categoryID;
	}

	public Object getCategoryAlias() {
		return this.categoryAlias;
	}

	public void setCategoryAlias(Object categoryAlias) {
		this.categoryAlias = categoryAlias;
	}

	public short getCategoryLevel() {
		return this.categoryLevel;
	}

	public void setCategoryLevel(short categoryLevel) {
		this.categoryLevel = categoryLevel;
	}

	public Object getCategoryName() {
		return this.categoryName;
	}

	public void setCategoryName(Object categoryName) {
		this.categoryName = categoryName;
	}

	public Object getCategoryName_EN() {
		return this.categoryName_EN;
	}

	public void setCategoryName_EN(Object categoryName_EN) {
		this.categoryName_EN = categoryName_EN;
	}

	public Object getCategoryOrder() {
		return this.categoryOrder;
	}

	public void setCategoryOrder(Object categoryOrder) {
		this.categoryOrder = categoryOrder;
	}

	public Integer getCategoryParentID() {
		return this.categoryParentID;
	}

	public void setCategoryParentID(Integer categoryParentID) {
		this.categoryParentID = categoryParentID;
	}

	public Object getCategoryText() {
		return this.categoryText;
	}

	public void setCategoryText(Object categoryText) {
		this.categoryText = categoryText;
	}

	public String getMsreplTranVersion() {
		return this.msreplTranVersion;
	}

	public void setMsreplTranVersion(String msreplTranVersion) {
		this.msreplTranVersion = msreplTranVersion;
	}

}