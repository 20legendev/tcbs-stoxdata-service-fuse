package vn.tcbs.api.stoxservice;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.MessageContext;
import org.springframework.beans.factory.annotation.Autowired;

import vn.tcbs.api.base.ResponseData;
import vn.tcbs.api.stoxservice.entity.Category;
import vn.tcbs.api.stoxservice.entity.Message;
import vn.tcbs.api.stoxservice.object.MsgRequest;
import vn.tcbs.api.stoxservice.service.IPartnerService;
import vn.tcbs.api.stoxservice.service.MessageService;

@WebService
@Path("/news")
public class NewsService implements INewsService {

	@Context
	private MessageContext context;
	@Autowired
	private MessageService messageService;
	@Autowired
	private IPartnerService partnerService;

	@POST
	@Path("/list-category")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON,
			MediaType.APPLICATION_FORM_URLENCODED })
	public Response getCategory() {
		List<Category> data = messageService.listCategory();
		ResponseData<List<Category>> res = new ResponseData<List<Category>>(0,
				data, "SUCCESS");
		return Response.ok(res).header("Access-Control-Allow-Origin", "*")
				.build();
	}

	@POST
	@Path("/list")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON,
			MediaType.APPLICATION_FORM_URLENCODED })
	public Response getNewsList(@FormParam("") MsgRequest req) {

		String partner = req.getPartner();
		Integer category = req.getCategory();
		String ticker = req.getTicker();
		String language = req.getLanguage();
		String date = req.getBydate();
		Date byDate = null;
		if (date != null) {
			date = date.replace("/", "-");
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			try {
				byDate = df.parse(date);
			} catch (Exception ex) {
				ResponseData<String> res = new ResponseData<String>(1, null,
						"DATE_FORMAT_ERROR");
				return Response.ok(res)
						.header("Access-Control-Allow-Origin", "*").build();
			}
		}

		Long lastId;
		Boolean isRestrict = false;
		String key = partner + ":" + category + ":" + ticker + ":" + language;
		if (partner != null && !partner.equals("")) {
			lastId = partnerService.getLastValue(partner, key);
			isRestrict = true;
		} else {
			lastId = null;
		}
		List<Message> news = messageService.getMessageList(req.getPage(),
				req.getPerpage(), category, ticker, language, byDate);

		/*
		 * if (news.size() > 0 && isRestrict) { Message lastMessage =
		 * news.get(0); if (lastId == null || lastId <
		 * lastMessage.getMessageID()) { partnerService.updateLastId(partner,
		 * key, lastMessage.getMessageID()); } }
		 */

		ResponseData<List<Message>> res = new ResponseData<List<Message>>(0,
				news, "SUCCESS");

		return Response.ok(res).header("Access-Control-Allow-Origin", "*")
				.build();
	}

	@POST
	@Path("/count")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON,
			MediaType.APPLICATION_FORM_URLENCODED })
	public Response getNewsCount(@FormParam("") MsgRequest req) {

		Integer category = req.getCategory();
		String ticker = req.getTicker();
		String language = req.getLanguage();
		String date = req.getBydate();
		Date byDate = null;
		if (date != null) {
			date = date.replace("/", "-");
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			try {
				byDate = df.parse(date);
			} catch (Exception ex) {
				ResponseData<String> res = new ResponseData<String>(1, null,
						"DATE_FORMAT_ERROR");
				return Response.ok(res)
						.header("Access-Control-Allow-Origin", "*").build();
			}
		}
		Long count = messageService.countMessage(category, ticker, language,
				byDate);
		ResponseData<Long> res = new ResponseData<Long>(0, count, "SUCCESS");
		return Response.ok(res).header("Access-Control-Allow-Origin", "*")
				.build();
	}

	@POST
	@Path("/detail")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON,
			MediaType.APPLICATION_FORM_URLENCODED })
	public Response getNewsDetail(@FormParam("id") Long messageId) {
		Message news = messageService.getMessageDetail(messageId);
		ResponseData<Message> res = new ResponseData<Message>(0, news,
				"SUCCESS");
		return Response.ok(res).header("Access-Control-Allow-Origin", "*")
				.build();
	}

	@GET
	@Path("/test")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON,
			MediaType.APPLICATION_FORM_URLENCODED })
	public Response test() {
		return Response.ok("Hello world")
				.header("Access-Control-Allow-Origin", "*").build();
	}

}
