package vn.tcbs.api.stoxservice.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.tcbs.api.stoxservice.dao.CompanyDAO;
import vn.tcbs.api.stoxservice.dao.ReportDAO;
import vn.tcbs.api.stoxservice.entity.Company;
import vn.tcbs.api.stoxservice.entity.DividendYield;

@Service("reportService")
public class ReportServiceImpl implements ReportService {

	@Autowired
	private ReportDAO reportDAO;
	@Autowired
	private CompanyDAO companyDAO;

	@Override
	public Map<String, Object> getReportByCompany(String code, int year, int quarter) {
		// TODO Auto-generated method stub
		List<Map<String, Object>> data = reportDAO.getByCompany(code, year, quarter);
		if(data.size() > 0){
			Map<String, Object> tmp = data.get(0);
			if(tmp.get("Nam") == null){
				return null;
			}
			return data.get(0);
		}else{
			return null;
		}
	}

	@Override
	public DividendYield getCashDiv(String code, int year) {
		// TODO Auto-generated method stub
		List<DividendYield> list = reportDAO.getCashDiv(code, year);
		return list.size() > 0 ? list.get(0) : null;
	}

	@Override
	public List<Company> listCompany(
			String search) {
		return companyDAO.listCompany(search);
	}

}
