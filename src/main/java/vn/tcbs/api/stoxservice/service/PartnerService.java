package vn.tcbs.api.stoxservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.tcbs.api.stoxservice.dao.IPartnerDAO;
import vn.tcbs.api.stoxservice.entity.TcbsPartner;

@Service("partnerService")
public class PartnerService implements IPartnerService {

	@Autowired
	private IPartnerDAO partnerDAO;

	@Override
	public Long getLastValue(String partner, String key) {
		// TODO Auto-generated method stub
		TcbsPartner partnerObj = partnerDAO.getByPartner(partner, key);
		if (partnerObj != null) {
			return partnerObj.getKeyValue();
		}
		return null;
	}

	@Override
	public void updateLastId(String partner, String key, Long lastId) {
		partnerDAO.update(partner, key, lastId);
	}

}
