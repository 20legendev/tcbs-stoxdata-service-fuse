package vn.tcbs.api.stoxservice.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.tcbs.api.stoxservice.dao.ICategoryDAO;
import vn.tcbs.api.stoxservice.dao.IPartnerDAO;
import vn.tcbs.api.stoxservice.dao.MessageDAO;
import vn.tcbs.api.stoxservice.entity.Category;
import vn.tcbs.api.stoxservice.entity.Message;

@Service("messageService")
public class MessageServiceImpl implements MessageService {

	@Autowired
	private MessageDAO messageDAO;
	@Autowired
	private ICategoryDAO categoryDAO;

	@Override
	public List<Message> getMessageList(Integer page, Integer perpage,
			Integer categoryId, String ticker, String language, Date date) {
		page = page == null ? 0 : page;
		perpage = perpage == null ? 100 : perpage;
		List<Message> messages = messageDAO.getMessage(page, perpage,
				categoryId, ticker, language, date);
		return messages;
	}

	@Override
	public Long countMessage(Integer categoryId, String ticker,
			String language, Date date) {
		return messageDAO.countMessage(categoryId, ticker, language, date);
	}

	@Override
	public Message getMessageDetail(Long messageID) {
		return messageDAO.getMessageById(messageID);
	}

	@Override
	public List<Category> listCategory() {
		return categoryDAO.listCategory();
	}
}
