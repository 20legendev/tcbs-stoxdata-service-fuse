package vn.tcbs.api.stoxservice.service;

import java.util.List;
import java.util.Map;

import vn.tcbs.api.stoxservice.entity.Company;
import vn.tcbs.api.stoxservice.entity.DividendYield;

public interface ReportService {

	public Map<String, Object> getReportByCompany(String code, int year, int quarter);
	public DividendYield getCashDiv(String code, int year);
	public List<Company> listCompany(String search);
}