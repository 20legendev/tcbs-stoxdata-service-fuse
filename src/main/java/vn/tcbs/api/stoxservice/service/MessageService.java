package vn.tcbs.api.stoxservice.service;

import java.util.Date;
import java.util.List;

import vn.tcbs.api.stoxservice.entity.Category;
import vn.tcbs.api.stoxservice.entity.Message;

public interface MessageService {
	public List<Message> getMessageList(Integer page, Integer perpage,
			Integer categoryId, String ticker, String language, Date date);

	public Message getMessageDetail(Long messageId);

	public List<Category> listCategory();

	public Long countMessage(Integer categoryId, String ticker,
			String language, Date date);
}