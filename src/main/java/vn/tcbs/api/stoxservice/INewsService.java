package vn.tcbs.api.stoxservice;

import javax.jws.WebService;
import javax.ws.rs.FormParam;
import javax.ws.rs.core.Response;

import vn.tcbs.api.stoxservice.object.MsgRequest;

@WebService
public interface INewsService {
	public Response getNewsList(@FormParam("") MsgRequest req);

	public Response getNewsDetail(@FormParam("id") Long messageId);
}
