package vn.tcbs.api.stoxservice.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import vn.tcbs.api.base.BaseDAO;
import vn.tcbs.api.stoxservice.entity.Category;

@Repository("categoryDAO")
@Transactional
public class CategoryDAO extends BaseDAO implements ICategoryDAO {

	@Override
	public List<Category> listCategory() {
		Criteria q = getSession().createCriteria(Category.class);
		return q.list();
	}
}