package vn.tcbs.api.stoxservice.dao;

import vn.tcbs.api.stoxservice.entity.TcbsPartner;

public interface IPartnerDAO {
	public TcbsPartner getByPartner(String partner, String key);
	public void update(String partner, String key, Long lastId);
}
