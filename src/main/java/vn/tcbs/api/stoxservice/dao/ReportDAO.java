package vn.tcbs.api.stoxservice.dao;

import java.util.List;
import java.util.Map;

import vn.tcbs.api.stoxservice.entity.DividendYield;

public interface ReportDAO {
	public List<Map<String, Object>> getByCompany(String code, int year, int quarter);
	public List<DividendYield> getCashDiv(String code, int year);
}