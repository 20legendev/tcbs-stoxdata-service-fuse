package vn.tcbs.api.stoxservice.dao;

import java.util.List;

import vn.tcbs.api.stoxservice.entity.Category;

public interface ICategoryDAO {
	public List<Category> listCategory();
}