package vn.tcbs.api.stoxservice.dao;

import java.util.List;

import vn.tcbs.api.stoxservice.entity.Company;

public interface CompanyDAO {
	public List<Company> listCompany(String search);
}