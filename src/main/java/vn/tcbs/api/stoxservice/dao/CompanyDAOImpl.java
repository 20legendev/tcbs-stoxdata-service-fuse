package vn.tcbs.api.stoxservice.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.stereotype.Repository;

import vn.tcbs.api.base.BaseDAO;
import vn.tcbs.api.stoxservice.entity.Company;

@Repository("companyDAO")
@Transactional
public class CompanyDAOImpl extends BaseDAO implements CompanyDAO {

	@Override
	public List<Company> listCompany(String search) {
		Query q = null;
		String select = "id, ticker, shortName,registedOffice, districtID, cityID";
		if (search != "") {
			q = getSession().createQuery("SELECT " + select + " FROM Company");
		} else {
			q = getSession()
					.createQuery(
							"SELECT "
									+ select
									+ " FROM Company WHERE shortName like :likeString' ")
					.setString("likeString", "%" + search + "%");
		}
		q.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		return q.list();
	}
}